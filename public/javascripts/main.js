const addButton = document.getElementById('addButton');

let phoneDirectory = [];

const deleteRow = async (id) => {
  const response = await fetch(`/api/contact/${id}`, { method: 'DELETE' });
  await response.text();

  fetchRows();
};

const renderRows = () => {
  const tbody = document.getElementById('tbody');

  tbody.innerHTML = '';

  const rows = phoneDirectory.map(({
    id, name, phone, email, address,
  }) => {
    const row = document.createElement('tr');

    const nameField = document.createElement('td');
    nameField.innerText = name;
    row.appendChild(nameField);
    const phoneField = document.createElement('td');
    phoneField.innerText = phone;
    row.appendChild(phoneField);
    const emailField = document.createElement('td');
    emailField.innerText = email;
    row.appendChild(emailField);

    const addressField = document.createElement('td');
    addressField.innerText = address;
    // addressField.dblclick = () => hola(document.getElementById('addressField'));
addressField.contentEditable=true;
    row.appendChild(addressField);

    const buttonField = document.createElement('td');
    const button = document.createElement('button');
    button.innerText = 'Remove';
    button.onclick = () => deleteRow(id);

    buttonField.appendChild(button);
    row.appendChild(buttonField);

    return row;
  });

  rows.forEach((row) => {
    tbody.appendChild(row);
  });
};

const fetchRows = async () => {
  const response = await fetch('/api/contacts');
  const phoneDirectoryArray = await response.json();

  phoneDirectory = [];
  phoneDirectory = phoneDirectoryArray;

  renderRows();
};

const addNewContact = async ({ name, phone, email, address}) => {
  const response = await fetch('/api/contact', {
    method: 'PUT',
    body: JSON.stringify({ name, phone, email, address }),
    headers: {
      'Content-Type': 'application/json',
    },
  });
  const newContact = await response.json();

  phoneDirectory.push(newContact);

  renderRows();
};

addButton.onclick = () => {
  const iName = document.getElementById('iName');
  const iPhone = document.getElementById('iPhone');
  const iEmail = document.getElementById('iEmail');
  const iAddress = document.getElementById('iAddress');

  const name = iName.value;
  const phone = iPhone.value;
  const email = iEmail.value;
  const address = iAddress.value;

  iName.value = '';
  iPhone.value = '';
  iEmail.value = '';
  iAddress.value = '';


  addNewContact({ name, phone, email, address });
};

// const hola =  async (addressField) => {
//   addressField.contentEditable = true;
//     setTimeout(function() {
//       if (document.activeElement !== addressField) {
//         addressField.contentEditable = false;
//       }
//     }, 300);
//   fetchRows();
// };

fetchRows();
